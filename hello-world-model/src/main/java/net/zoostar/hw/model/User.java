package net.zoostar.hw.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class User {

	private String username;
	
	private String email;
	
	private String name;
	
}
