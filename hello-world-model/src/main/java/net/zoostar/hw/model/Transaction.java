package net.zoostar.hw.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Transaction {

	private Date date;
	
	private String payee;
	
	private Account account;
	
	private Category category;
	
	private String memo;
	
	private float quantity;
	
	private float amount;
}
