package net.zoostar.hw.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubCategory extends Category {

	private CategoryType type;
	
}
