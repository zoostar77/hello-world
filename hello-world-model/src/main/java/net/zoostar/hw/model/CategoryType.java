package net.zoostar.hw.model;

public enum CategoryType {
	EXPENSE, INCOME
}
