package net.zoostar.hw.model;

import java.util.Collection;

public interface Hierarchical<T> {
	int ROOT_LEVEL = 0;
	T getParent();
	Collection<T> getChildren();
	boolean addChild(T child);
	int getLevel();
	boolean isRoot();
}
