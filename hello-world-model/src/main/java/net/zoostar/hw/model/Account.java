package net.zoostar.hw.model;

import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account extends Category {

	private AccountType type;
	
	private Collection<Transaction> transactions;
}
