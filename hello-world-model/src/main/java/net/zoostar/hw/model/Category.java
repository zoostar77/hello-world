package net.zoostar.hw.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Category {

	private String name;
	
}
