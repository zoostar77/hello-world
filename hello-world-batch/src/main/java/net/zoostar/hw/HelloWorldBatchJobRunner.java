package net.zoostar.hw;

import org.springframework.batch.core.launch.support.CommandLineJobRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.Generated;

@Generated
@SpringBootApplication
public class HelloWorldBatchJobRunner extends CommandLineJobRunner {

	public static void main(String[] args) {
		var context = SpringApplication.run(HelloWorldBatchJobRunner.class, args);
		int code = SpringApplication.exit(context);
		System.exit(code);
	}
}
