package net.zoostar.hw.batch;

import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableBatchProcessing
public class GreetingJobConfigurer {

	private boolean readingComplete = false;
	
	@Bean
	ItemReader<String> reader() {
		return new ItemReader<String>() {

			@Override
			public String read()
					throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
				if(!readingComplete) {
					readingComplete = true;
					return "Hello World!";
				} else {
					return null;
				}
			}
			
		};
	}
	
	@Bean
	ItemWriter<String> writer() {
		return new ItemWriter<String>() {

			@Override
			public void write(List<? extends String> items) throws Exception {
				for(String item : items) {
					log.info("{}", item);
				}
			}
			
		};
	}
	
	@Bean
	@JobScope
	Step step(JobRepository jobRepository, PlatformTransactionManager transactionManager, ItemReader<String> reader,
			ItemWriter<String> writer) {
		return new StepBuilder("step").<String, String>chunk(3).reader(reader).writer(writer).repository(jobRepository)
				.transactionManager(transactionManager).build();
	}

	@Bean
	Job job(JobRepository jobRepository, Step step) {
		return new JobBuilder("job").start(step).repository(jobRepository).build();
	}
}
