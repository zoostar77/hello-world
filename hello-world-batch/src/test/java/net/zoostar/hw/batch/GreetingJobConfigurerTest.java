package net.zoostar.hw.batch;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import net.zoostar.hw.HelloWorldBatchJobRunner;

@SpringBatchTest
@SpringJUnitConfig(HelloWorldBatchJobRunner.class)
class GreetingJobConfigurerTest {
 
	@Autowired
    JobLauncherTestUtils jobLauncherTestUtils;
	
	@Autowired
	Job job;
 
	@Test
	void testJob() throws Exception {
		jobLauncherTestUtils.setJob(job);
		var execution = jobLauncherTestUtils.launchJob();
		Assertions.assertThat(execution.getExitStatus()).isEqualTo(ExitStatus.COMPLETED);
	}

}
