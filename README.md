# Hello World

## Environment URLs (https)
- Context: /hw
- DEV: dev.zoostar.net/hw
- INT: int.zoostar.net/hw
- UAT: uat.zoostar.net/hw
- Internal PROD: prod.zoostar.net/hw
- External PROD: hw.zoostar.net / www.hw.com

## Naming Conventions
### Controllers
Always start with the Http Method in the name.
- Collection<UserEntity> getUsers()
- UserEntity getUser(String name)
- UserEntity postUser(User user)

### Service
Always use CRUD naming conventions for naming such as:
- UserEntity create(User user)
- UserEntity retrieve(String name)
- UserEntity update(User user)
- UserEntity delete(String name)

### Model
Always return Entities instead of Models from RestfulControllers.
There could be a need to decorate values and also use the persisted entity ID for downstream purposes which are not usually available in Model classes.