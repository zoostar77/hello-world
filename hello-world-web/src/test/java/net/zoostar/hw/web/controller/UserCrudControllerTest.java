package net.zoostar.hw.web.controller;

import static net.zoostar.hw.service.impl.UserCrudServiceImpl.DEFAULT_PAGE_SIZE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;

import com.fasterxml.jackson.core.type.TypeReference;

import lombok.extern.slf4j.Slf4j;
import net.zoostar.hw.dao.kd.UserRepository;
import net.zoostar.hw.entity.UserEntity;
import net.zoostar.hw.model.User;
import net.zoostar.hw.request.StringIdRequest;
import net.zoostar.hw.request.UserRequest;

@Slf4j
class UserCrudControllerTest extends AbstractMockContainer<UserRepository, UserEntity, String, String> {

	User user;

	UserRequest userRequest;

	UserEntity persistable;

	UserEntity entity;

	List<UserEntity> entities = new ArrayList<>();

	String id = UUID.randomUUID().toString();

	@BeforeEach
	void beforeEach() {
		user = new User();
		user.setEmail("email@zoostar.net");
		user.setName("Name");
		user.setUsername("username");

		userRequest = new UserRequest(user);
		persistable = new UserEntity(user);
		entity = new UserEntity(user);
		entity.setId(id);

		entities.add(entity);
	}

	@Test
	void testPostUser201() throws Exception {
		// given
		String url = "/api/user";
		when(repository.save(persistable)).thenReturn(entity);

		// when
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON)
				.content(om.writeValueAsString(userRequest)).accept(APPLICATION_JSON)).andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
		entity = om.readValue(response.getContentAsString(), UserEntity.class);
		assertThat(entity).isNotNull();
		log.info("Created new user entity: {}", entity);

		assertThat(persistable.isNew()).isTrue();
		assertThat(persistable).isEqualTo(entity).hasSameHashCodeAs(entity);

		assertThat(entity.isNew()).isFalse();
		assertThat(entity.getEmail()).isEqualTo(userRequest.getEmail());
		assertThat(entity.getUsername()).isEqualTo(userRequest.getUsername());
		assertThat(entity.getName()).isEqualTo(userRequest.getName());
		assertThat(entity.getKey()).isEqualTo(userRequest.getUsername());
	}

	@Test
	void testPostForUserById200() throws Exception {
		// given
		String url = "/api/user/id";
		var request = new StringIdRequest(id);
		when(repository.findById(id)).thenReturn(Optional.ofNullable(entity));

		// when
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON)
				.accept(APPLICATION_JSON).content(om.writeValueAsString(request))).andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		entity = om.readValue(response.getContentAsString(), UserEntity.class);
		assertThat(entity).isNotNull();
		log.info("Created new user entity: {}", entity);

		assertThat(persistable.isNew()).isTrue();
		assertThat(persistable).isEqualTo(entity).hasSameHashCodeAs(entity);

		assertThat(entity.isNew()).isFalse();
		assertThat(entity.getEmail()).isEqualTo(userRequest.getEmail());
		assertThat(entity.getUsername()).isEqualTo(userRequest.getUsername());
		assertThat(entity.getName()).isEqualTo(userRequest.getName());
		assertThat(entity.getKey()).isEqualTo(userRequest.getUsername());
	}

	@Test
	void testGetUserByKey200() throws Exception {
		// given
		String url = "/api/user/key?username=" + user.getUsername();
		when(repository.findByKey(user.getUsername())).thenReturn(Optional.ofNullable(entity));

		// when
		MockHttpServletResponse response = service.perform(get(url).accept(APPLICATION_JSON)).andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		entity = om.readValue(response.getContentAsString(), UserEntity.class);
		assertThat(entity).isNotNull();
		log.info("Created new user entity: {}", entity);

		assertThat(persistable.isNew()).isTrue();
		assertThat(persistable).isEqualTo(entity).hasSameHashCodeAs(entity);

		assertThat(entity.isNew()).isFalse();
		assertThat(entity.getEmail()).isEqualTo(userRequest.getEmail());
		assertThat(entity.getUsername()).isEqualTo(userRequest.getUsername());
		assertThat(entity.getName()).isEqualTo(userRequest.getName());
		assertThat(entity.getKey()).isEqualTo(userRequest.getUsername());

		var that = entity;
		assertThat(entity).isEqualTo(that).isNotEqualTo(user);
	}

	@Test
	void testGetUserByKey400() throws Exception {
		// given
		String url = "/api/user/key?username=unknown";
		when(repository.findByKey(user.getUsername())).thenReturn(Optional.empty());

		// when
		MockHttpServletResponse response = service.perform(get(url).accept(APPLICATION_JSON)).andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	void testPostUser400MissingKey() throws Exception {

		userRequest.setUsername("");

		// given
		String url = "/api/user";

		// then
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON)
				.content(om.writeValueAsString(userRequest)).accept(APPLICATION_JSON)).andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	void testPostUser400InvalidEmail() throws Exception {

		userRequest.setEmail("invalidEmailFormat");

		// given
		String url = "/api/user";

		// then
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON)
				.content(om.writeValueAsString(userRequest)).accept(APPLICATION_JSON)).andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	void testGetUsers200() throws Exception {
		// given
		int pageNum = 0;
		String url = "/api/user/" + pageNum;
		PageRequest pageRequest = PageRequest.of(pageNum, DEFAULT_PAGE_SIZE);
		var page = new PageImpl<>(entities, pageRequest, DEFAULT_PAGE_SIZE);
		when(repository.findAll(pageRequest)).thenReturn(page);

		// when
		MockHttpServletResponse response = service.perform(get(url).accept(APPLICATION_JSON)).andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		Collection<UserEntity> users = om.readValue(response.getContentAsString(),
				new TypeReference<Collection<UserEntity>>() {
				});
		assertThat(users).isNotEmpty();
	}

	@Test
	void testGetUsers400() throws Exception {

		entities.clear();

		// given
		int pageNum = 0;
		String url = "/api/user/" + pageNum;
		PageRequest pageRequest = PageRequest.of(pageNum, DEFAULT_PAGE_SIZE);
		var page = new PageImpl<>(entities, pageRequest, DEFAULT_PAGE_SIZE);
		when(repository.findAll(pageRequest)).thenReturn(page);

		// when
		MockHttpServletResponse response = service.perform(get(url).accept(APPLICATION_JSON)).andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	void testPostDuplicateUser400() throws Exception {
		// given
		String url = "/api/user";
		when(repository.findByKey(persistable.getKey())).thenReturn(Optional.of(entity));

		// when
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON)
				.content(om.writeValueAsString(userRequest)).accept(APPLICATION_JSON)).andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	void testPutUser200() throws Exception {
		userRequest.setName("Updated Name");
		persistable.setId(entity.getId());
		persistable.setName(userRequest.getName());

		// given
		String url = "/api/user";
		when(repository.save(persistable)).thenReturn(persistable);
		when(repository.findByKey(userRequest.toKeyBasedPersistable().getKey())).thenReturn(Optional.of(entity));

		// when
		MockHttpServletResponse response = service.perform(
				put(url).contentType(APPLICATION_JSON).content(om.writeValueAsString(userRequest)).accept(APPLICATION_JSON))
				.andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		entity = om.readValue(response.getContentAsString(), UserEntity.class);
		assertThat(entity).isNotNull();
		log.info("Updated existing entity: {}", entity);

		assertThat(persistable).isEqualTo(entity).hasSameHashCodeAs(entity);

		assertThat(entity.isNew()).isFalse();
		assertThat(entity.getEmail()).isEqualTo(userRequest.getEmail());
		assertThat(entity.getUsername()).isEqualTo(userRequest.getUsername());
		assertThat(entity.getName()).isEqualTo(userRequest.getName());
		assertThat(entity.getKey()).isEqualTo(userRequest.getUsername());
	}

	@Test
	void testPutUserNotFound400() throws Exception {
		userRequest.setName("Updated Name");
		persistable.setId(entity.getId());
		persistable.setName(userRequest.getName());

		// given
		String url = "/api/user";
		when(repository.findByKey(userRequest.toKeyBasedPersistable().getKey())).thenReturn(Optional.empty());

		// when
		MockHttpServletResponse response = service.perform(
				put(url).contentType(APPLICATION_JSON).content(om.writeValueAsString(userRequest)).accept(APPLICATION_JSON))
				.andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	void testPostUserMissingEmail400() throws Exception {
		userRequest.setEmail("");

		// given
		String url = "/api/user";

		// when
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON)
				.content(om.writeValueAsString(userRequest)).accept(APPLICATION_JSON)).andReturn().getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

}
