package net.zoostar.hw.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.zoostar.hw.HelloWorldWeb;
import net.zoostar.kd.data.KeyBasedRepository;
import net.zoostar.kd.domain.KeyBasedPersistable;

@AutoConfigureMockMvc
@SpringBootTest(classes = { HelloWorldWeb.class })
public abstract class AbstractMockContainer<R extends KeyBasedRepository<E, K, I>, E extends KeyBasedPersistable<K, I>, K, I> {

	@Autowired
	MockMvc service;

	@Autowired
	ObjectMapper om;

	@MockBean
	R repository;
}
