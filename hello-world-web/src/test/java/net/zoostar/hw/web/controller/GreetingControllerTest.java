package net.zoostar.hw.web.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.zoostar.hw.HelloWorldWeb;
import net.zoostar.hw.request.StringRequest;

@AutoConfigureMockMvc
@SpringBootTest(classes = { HelloWorldWeb.class })
class GreetingControllerTest {

	@Autowired
	MockMvc service;

	@Autowired
	ObjectMapper om;

	@Test
	void testPostGreet200() throws JsonProcessingException, Exception {
		String message = "World!";

		// given
		StringRequest request = new StringRequest(message);
		String url = "/api/greet";

		// when
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON_VALUE)
				.content(om.writeValueAsString(request)).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn()
				.getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		StringRequest value = om.readValue(response.getContentAsString(), StringRequest.class);
		assertThat(value.toString()).hasToString("Hello " + request.toString());
		assertThat(value.getValue()).isEqualTo("Hello " + message);
	}

	@Test
	void testPostGreet400() throws JsonProcessingException, Exception {
		String message = "";

		// given
		StringRequest request = new StringRequest(message);
		String url = "/api/greet";

		// when
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON_VALUE)
				.content(om.writeValueAsString(request)).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn()
				.getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		StringRequest value = om.readValue(response.getContentAsString(), StringRequest.class);
		assertThat(value.getValue()).contains("Required field may not be blank!");
	}

	@Test
	void testGetGreet200() throws JsonProcessingException, Exception {
		StringRequest message = new StringRequest("Hello World!");

		// given
		String url = "/api/greet";

		// then
		MockHttpServletResponse response = service.perform(get(url).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		StringRequest value = om.readValue(response.getContentAsString(), StringRequest.class);
		assertThat(value.toString()).hasToString(message.toString());
		assertThat(value).hasSameHashCodeAs(message);
	}

	@Test
	void testPostGreetings200() throws Exception {
		// given
		Collection<StringRequest> names = new ArrayList<>();
		names.add(new StringRequest("Joe"));
		names.add(new StringRequest("Jill"));
		names.add(new StringRequest("Jenny"));
		names.add(new StringRequest("Jack"));
		names.add(new StringRequest("Jim"));
		names.add(new StringRequest("John"));
		String url = "/api/greetings";

		// when
		MockHttpServletResponse response = service.perform(post(url).contentType(APPLICATION_JSON_VALUE)
				.content(om.writeValueAsString(names)).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn()
				.getResponse();

		// then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

	}

}
