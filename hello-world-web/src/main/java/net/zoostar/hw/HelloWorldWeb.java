package net.zoostar.hw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.Generated;

@Generated
@SpringBootApplication
public class HelloWorldWeb extends SpringBootServletInitializer implements WebMvcConfigurer, AsyncConfigurer {

	public static final String ACTIVE_PROFILES_NAME = "spring.profiles.active";
	public static final String HW_PROFILE_NAME = "hw.profile";
	public static final String DEFAULT_PROFILE_VALUE = "local";

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(HelloWorldWeb.class);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/", "/swagger-ui/index.html");
		WebMvcConfigurer.super.addViewControllers(registry);
	}

	public static void main(String[] args) {
		System.setProperty(ACTIVE_PROFILES_NAME, System.getProperty(HW_PROFILE_NAME, DEFAULT_PROFILE_VALUE));
		SpringApplication.run(HelloWorldWeb.class, args);
	}

}
