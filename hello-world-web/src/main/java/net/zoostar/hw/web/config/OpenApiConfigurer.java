package net.zoostar.hw.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class OpenApiConfigurer {

	public static final String PROPERTY_DELIM_VALUE = ",";

	@Bean
	OpenAPI openAPI(@Autowired Environment environment, @Autowired BuildProperties bp) {
		return new OpenAPI().info(new Info().title("Hello World")
				.description("Hello World API Services for Profiles(s): "
						+ String.join(PROPERTY_DELIM_VALUE, environment.getActiveProfiles()))
				.contact(new Contact().name("zoostar").email("devops@zoostar.net"))
				.license(new License().name("MIT License")
						.url("https://bitbucket.org/zoostar77/zoostar-boot-starter-parent/src/master/LICENSE.md"))
				.version(bp.getVersion() + "." + bp.getTime()));
	}

}
