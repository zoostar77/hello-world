package net.zoostar.hw.web.controller;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import net.zoostar.hw.request.StringRequest;
import net.zoostar.hw.service.GreetingService;

@Slf4j
@Validated
@RestController
@Secured({ "ROLE_USER" })
public class GreetingController {

	@Autowired
	protected GreetingService greetingManager;

	@GetMapping(path = "/api/greet", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringRequest> getGreeting() {
		log.debug("{}", "Hello World!");
		return new ResponseEntity<>(new StringRequest("Hello World!"), HttpStatus.OK);
	}

	@PostMapping(path = "/api/greet", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringRequest> postGreeting(@RequestBody @Valid StringRequest value, BindingResult errors) {
		log.info("Received a POST request for greet value: {}...", value);
		return ResponseEntity.ok(new StringRequest(greetingManager.retrieve(value.getValue())));
	}

	@PostMapping(path = "/api/greetings", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> postGreetings(@RequestBody Collection<StringRequest> names, BindingResult errors) {
		log.info("Received a POST request for greeting {} people...", names.size());
		Collection<String> listOfNames = new ArrayList<>(names.size());
		names.forEach(name -> listOfNames.add(name.toString()));
		greetingManager.greet(listOfNames);
		log.info("Triggered job for greeting {} people.", names.size());
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
