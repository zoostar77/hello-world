package net.zoostar.hw.web.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableAsync;

import lombok.Generated;

@Generated
@EnableAsync
@EnableCaching
@Profile("!test")
public class ApplicationContextConfigurer {

}
