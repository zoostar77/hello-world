package net.zoostar.hw.web.request;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import net.zoostar.hw.service.EntityCrudService;
import net.zoostar.kd.domain.KeyBasedPersistable;

@Getter
@Setter
@Component
public class PersistableCrudServiceFactoryImpl<T extends EntityCrudService<E, K, I>, E extends KeyBasedPersistable<K, I>, K, I>
		implements EntityCrudServiceFactory<T, E, K, I>, InitializingBean {

	private Map<String, T> persistableCrudServiceFactory = new HashMap<>();

	@Autowired
	private T persistableUserCrudService;

	@Override
	public T getInstance(String entityType) {
		return persistableCrudServiceFactory.get(entityType);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		persistableCrudServiceFactory.put("user", persistableUserCrudService);
	}

}
