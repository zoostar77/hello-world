package net.zoostar.hw.web.config;

import static org.springframework.security.config.Customizer.withDefaults;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

import lombok.Generated;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Generated
@Configuration
@EnableWebSecurity
public class OAuthSecurityConfigurer {

	public static final String PROFILE_SECURED = "secured";

	@Value("${profile.secured:false}")
	protected boolean secured;
	
	@Value("${profile.local}")
	protected boolean local;
	
	@Value("${profile.test:false}")
	protected boolean test;

	@Bean
	SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		log.info("{}", "Configuring OAuth Security based on Profile...");
		
		if (secured) {
			log.info("{}...", "Securing application");
            http.authorizeHttpRequests().antMatchers("/**").hasRole("USER").anyRequest().authenticated().and().oauth2Login(withDefaults());
		} else {
			http.authorizeHttpRequests().antMatchers("/**").permitAll();
		}
		
		if(local || test) {
            http.csrf(csrf -> csrf.disable());
		}
		
		return http.build();
	}

}
