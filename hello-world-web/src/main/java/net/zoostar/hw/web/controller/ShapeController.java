package net.zoostar.hw.web.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import net.zoostar.hw.request.shapes.Shape;

@Slf4j
@RestController
@RequestMapping("/api/shape/area")
public class ShapeController {

	@PostMapping(path = "/{shapeType}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Double> calculateArea(@PathVariable String shapeType, @RequestBody Shape shape) {
		log.info("Calculating area for shape: {}", shapeType);
		if(!shapeType.equalsIgnoreCase(shape.getClass().getSimpleName())) {
			throw new IllegalArgumentException("Invalid shape type found!");
		}
		return ResponseEntity.ok(shape.area());
	}
	
}
