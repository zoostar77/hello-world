package net.zoostar.hw.web.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import net.zoostar.hw.entity.KeyBasedPersistableEntityMapper;
import net.zoostar.hw.entity.PersistableIdMapper;
import net.zoostar.hw.entity.PersistableKeyMapper;
import net.zoostar.hw.service.EntityCrudService;
import net.zoostar.hw.web.request.EntityCrudServiceFactory;
import net.zoostar.kd.domain.KeyBasedPersistable;

@Slf4j
@RestController
@RequestMapping("/api/{entityType}")
public class GenericEntityCrudController<T extends EntityCrudService<E, K, I>, E extends KeyBasedPersistable<K, I>, K, I> {

	@Autowired
	protected EntityCrudServiceFactory<T, E, K, I> entityCrudServiceFactory;

	@PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<E> postPersistable(@PathVariable String entityType,
			@Valid @RequestBody KeyBasedPersistableEntityMapper<E, K, I> request) {
		log.info("Creating new entity {} via request: {}...", entityType, request);
		return new ResponseEntity<>(entityCrudServiceFactory.getInstance(entityType).create(request),
				HttpStatus.CREATED);
	}

	@PostMapping(path = "/id", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<E> postForEntityById(@PathVariable String entityType, @RequestBody PersistableIdMapper<I> id) {
		log.info("Retrieving entity {} via id: {}...", entityType, id);
		return ResponseEntity.ok(entityCrudServiceFactory.getInstance(entityType).retrieveById(id));
	}

	public ResponseEntity<E> postForEntityByKey(@PathVariable String entityType, @RequestBody PersistableKeyMapper<K> key) {
		log.info("Retrieving entity {} via key: {}...", entityType, key);
		return ResponseEntity.ok(entityCrudServiceFactory.getInstance(entityType).retrieveByKey(key));
	}
	
	@GetMapping(path = "/{page}", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<E>> getUsers(@PathVariable String entityType, @PathVariable int page) {
		log.info("Retrieving entities for page: {}...", page);
		return ResponseEntity
				.ok(entityCrudServiceFactory.getInstance(entityType).retrieve(page, new ArrayList<>()));
	}

	@PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<E> putPersistable(@PathVariable String entityType,
			@Valid @RequestBody KeyBasedPersistableEntityMapper<E, K, I> request) {
		log.info("Request received to update existing entity: {}...", request);
		return ResponseEntity.ok(entityCrudServiceFactory.getInstance(entityType).update(request));
	}

}
