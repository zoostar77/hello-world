package net.zoostar.hw.web.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.zoostar.hw.entity.TransactionEntity;
import net.zoostar.hw.model.Transaction;
import net.zoostar.kd.domain.PersistableEntityMapper;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public abstract class TransactionRequest extends Transaction implements PersistableEntityMapper<TransactionEntity, String> {

	private Transaction tx;
	
	public TransactionEntity toPersistable() {
		var persistable = new TransactionEntity();
		persistable.setAccount(tx.getAccount());
		persistable.setAmount(tx.getAmount());
		persistable.setCategory(tx.getCategory());
		persistable.setDate(tx.getDate());
		persistable.setMemo(tx.getMemo());
		persistable.setPayee(tx.getPayee());
		persistable.setQuantity(tx.getQuantity());
		return persistable;
	}
}
