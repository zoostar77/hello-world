package net.zoostar.hw.web.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.Getter;
import net.zoostar.hw.entity.PersistableKeyMapper;
import net.zoostar.hw.entity.UserEntity;
import net.zoostar.hw.service.UserCrudService;

@Getter
@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	protected GenericEntityCrudController<UserCrudService, UserEntity, String, String> genericEntityCrudController;
	
	@GetMapping(path = "/key", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<UserEntity> getByUsername(@RequestParam String username) {
		return genericEntityCrudController.postForEntityByKey("user", new PersistableKeyMapper<>() {

			@Override
			public String toKey() {
				return username;
			}
			
			@Override
			public String toString() {
				return username;
			}
			
		});
	}

}
