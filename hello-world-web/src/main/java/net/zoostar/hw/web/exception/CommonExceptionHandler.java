package net.zoostar.hw.web.exception;

import javax.validation.ValidationException;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;
import net.zoostar.hw.model.User;
import net.zoostar.hw.request.StringRequest;

@Slf4j
@ControllerAdvice
public class CommonExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	protected ResponseEntity<StringRequest> handleException(MethodArgumentNotValidException e) {
		return logAndReturn400(e);
	}
	
	@ExceptionHandler(ValidationException.class)
	protected ResponseEntity<StringRequest> handleException(ValidationException e) {
		return logAndReturn400(e);
	}

	@ExceptionHandler(IncorrectResultSizeDataAccessException.class)
	protected ResponseEntity<User> handleException(IncorrectResultSizeDataAccessException e) {
		log.warn("{}", e.getMessage());
		return new ResponseEntity<>(new User(), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(DuplicateKeyException.class)
	protected ResponseEntity<StringRequest> handleException(DuplicateKeyException e) {
		return logAndReturn400(e);
	}

	protected ResponseEntity<StringRequest> logAndReturn400(Exception e) {
		log.warn("{}", e.getMessage());
		return new ResponseEntity<>(new StringRequest(e.getMessage()), HttpStatus.BAD_REQUEST);
	}

}
