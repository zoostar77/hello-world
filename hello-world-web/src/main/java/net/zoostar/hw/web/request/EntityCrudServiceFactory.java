package net.zoostar.hw.web.request;

import net.zoostar.hw.service.EntityCrudService;
import net.zoostar.kd.domain.KeyBasedPersistable;

public interface EntityCrudServiceFactory<T extends EntityCrudService<E, K, I>, E extends KeyBasedPersistable<K, I>, K, I> {
	T getInstance(String entityType);
}
