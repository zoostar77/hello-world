package net.zoostar.hw.validation;

public interface Validator<T> {
	void validate(T object);
}
