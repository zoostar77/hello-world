package net.zoostar.hw.validation.impl;

import javax.validation.ValidationException;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import net.zoostar.hw.model.User;
import net.zoostar.hw.validation.Validator;

@Component
public class UserValidator implements Validator<User> {

	@Override
	public void validate(User user) {
		if(!StringUtils.hasText(user.getEmail())) {
			throw new ValidationException("Missing Required Field: email");
		}
	}

}
