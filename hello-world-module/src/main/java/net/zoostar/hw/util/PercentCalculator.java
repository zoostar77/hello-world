package net.zoostar.hw.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PercentCalculator {

	private final int total;
	
	public float calculate(int part) {
		return (part * 100) / total;
	}
}
