package net.zoostar.hw.service;

import java.util.Collection;

import net.zoostar.hw.entity.KeyBasedPersistableEntityMapper;
import net.zoostar.hw.entity.PersistableIdMapper;
import net.zoostar.hw.entity.PersistableKeyMapper;
import net.zoostar.kd.domain.KeyBasedPersistable;

public interface EntityCrudService<T extends KeyBasedPersistable<K, I>, K, I> {
	T create(KeyBasedPersistableEntityMapper<T, K, I> mapper);
	T retrieveById(PersistableIdMapper<I> mapper);
	T retrieveByKey(PersistableKeyMapper<K> mapper);
	Collection<T> retrieve(int pageNum, Collection<T> entities);
	T update(KeyBasedPersistableEntityMapper<T, K, I> mapper);

}
