package net.zoostar.hw.service;

import java.util.Collection;

public interface GreetingService {
	String retrieve(String value);
	void greet(Collection<String> names);
}
