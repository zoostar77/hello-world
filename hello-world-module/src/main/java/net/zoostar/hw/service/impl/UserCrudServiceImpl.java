package net.zoostar.hw.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.Getter;
import net.zoostar.hw.dao.kd.UserRepository;
import net.zoostar.hw.entity.PersistableKeyMapper;
import net.zoostar.hw.entity.UserEntity;
import net.zoostar.hw.model.User;
import net.zoostar.hw.service.UserCrudService;
import net.zoostar.hw.validation.Validator;

@Getter
@Service
public class UserCrudServiceImpl extends AbstractEntityCrudService<UserEntity, String, String>
		implements UserCrudService {
	
	public static final int DEFAULT_PAGE_SIZE = 10;

	@Autowired
	protected Validator<User> userValidator;

	@Autowired
	protected UserRepository keyBasedRepository;
	
	@Override
	protected void preCreate(UserEntity persistable) {
		getUserValidator().validate(persistable);
	}

	@Override
	public UserEntity retrieveByUsername(String username) {
		return super.retrieveByKey(new PersistableKeyMapper<>() {

			@Override
			public String toKey() {
				return username;
			}
			
		});
	}

	@Override
	protected void setId(UserEntity persistable, String id) {
		persistable.setId(id);
	}

	@Override
	protected void setId(UserEntity persistable) {
		persistable.setId(UUID.randomUUID().toString());
	}

	@Override
	protected int getPageSize() {
		return DEFAULT_PAGE_SIZE;
	}

}
