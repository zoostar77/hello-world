package net.zoostar.hw.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import net.zoostar.hw.component.impl.GreetingProcessorImpl;
import net.zoostar.hw.service.GreetingService;

@Slf4j
@Service
public class GreetingServiceImpl implements GreetingService {

	@Autowired
	ThreadPoolTaskExecutor pooledTaskScheduler;
	
	@Override
	public String retrieve(String value) {
		return "Hello " + value;
	}

	@Async
	@Cacheable(value = "greetings")
	@Override
	public void greet(Collection<String> names) {
		int index = 0;
		List<Future<String>> futures = new ArrayList<>(names.size());

		List<String> listOfNames = new ArrayList<>(names);
		Collections.sort(listOfNames);
		for(String name : listOfNames) {
			log.debug("Triggering job {} of {}: {}...", ++index, listOfNames.size(), name);
			futures.add(pooledTaskScheduler.submit(new GreetingProcessorImpl(name)));
		}
		
		for(Future<String> future : futures) {
			try {
				log.info("{}", future.get());
			} catch (InterruptedException e) {
				log.warn(e.getMessage());
				Thread.currentThread().interrupt();
			} catch (ExecutionException e) {
				log.warn(e.getMessage());
			}
		}
		log.info("{}", "Completed processing ALL jobs.");
	}

}
