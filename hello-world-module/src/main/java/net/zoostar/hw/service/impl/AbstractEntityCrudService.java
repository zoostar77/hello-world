package net.zoostar.hw.service.impl;

import java.util.Collection;
import java.util.Optional;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import lombok.extern.slf4j.Slf4j;
import net.zoostar.hw.entity.KeyBasedPersistableEntityMapper;
import net.zoostar.hw.entity.PersistableIdMapper;
import net.zoostar.hw.entity.PersistableKeyMapper;
import net.zoostar.hw.service.EntityCrudService;
import net.zoostar.kd.data.KeyBasedRepository;
import net.zoostar.kd.domain.KeyBasedPersistable;

@Slf4j
public abstract class AbstractEntityCrudService<T extends KeyBasedPersistable<K, I>, K, I>
		implements EntityCrudService<T, K, I> {

	protected abstract void preCreate(T persistable);

	public final T create(KeyBasedPersistableEntityMapper<T, K, I> mapper) {
		var persistable = mapper.toKeyBasedPersistable();
		preCreate(persistable);
		if(getKeyBasedRepository().findByKey(persistable.getKey()).isPresent()) {
			throw new DuplicateKeyException("Entity for this key already exists!");
		}

		log.info("Saving persistable: {}...", persistable);
		setId(persistable);
		T entity = getKeyBasedRepository().save(persistable);
		return postCreate(entity);
	}

	@Override
	public T retrieveById(PersistableIdMapper<I> mapper) {
		I id = mapper.toId();
		return retrieveIfPresent(getKeyBasedRepository().findById(id), "No entity found for id: " + id);
	}

	@Override
	public T retrieveByKey(PersistableKeyMapper<K> mapper) {
		K key = mapper.toKey();
		return retrieveIfPresent(getKeyBasedRepository().findByKey(key), "No entity found for key: " + key);
	}

	protected abstract KeyBasedRepository<T, K, I> getKeyBasedRepository();

	protected T postCreate(T entity) {
		return entity;
	}

	protected T retrieveIfPresent(Optional<T> entity, String errorMessage) {
		if (!entity.isPresent()) {
			throw new IncorrectResultSizeDataAccessException(errorMessage, 1);
		}
		return entity.get();
	}

	@Override
	public Collection<T> retrieve(int pageNum, Collection<T> entities) {
		int pageSize = getPageSize();
		Page<T> page = getKeyBasedRepository().findAll(PageRequest.of(pageNum, pageSize));
		if(page.hasContent()) {
			page.forEach(entities::add);
		} else {
			throw new EmptyResultDataAccessException("No entities found for page " + pageNum, pageSize);
		}
		return entities;
	}

	protected abstract int getPageSize();

	@Override
	public final T update(KeyBasedPersistableEntityMapper<T, K, I> mapper) {
		var persistable = mapper.toKeyBasedPersistable();
		preUpdate(persistable);
		var object = getKeyBasedRepository().findByKey(persistable.getKey());
		if(!object.isPresent()) {
			throw new EmptyResultDataAccessException("No Entity found to update!", 1);
		}
		
		setId(persistable, object.get().getId());
		T entity = getKeyBasedRepository().save(persistable);
		return postCreate(entity);
	}

	protected void preUpdate(T persistable) {
		// TODO Auto-generated method stub
		
	}

	protected abstract void setId(T persistable, I id);

	protected abstract void setId(T persistable);
	
}
