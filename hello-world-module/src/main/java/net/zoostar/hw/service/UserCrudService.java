package net.zoostar.hw.service;

import net.zoostar.hw.entity.UserEntity;

public interface UserCrudService extends EntityCrudService<UserEntity, String, String> {

	UserEntity retrieveByUsername(String username);

}
