package net.zoostar.hw.service;

import net.zoostar.hw.model.Transaction;

public interface AccountService {
	Transaction create(Transaction tx);
}
