package net.zoostar.hw.component;

import java.util.UUID;

public interface JobStatus<T> {
	UUID getId();
	T getContent();
}
