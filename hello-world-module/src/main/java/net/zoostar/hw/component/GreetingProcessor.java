package net.zoostar.hw.component;

public interface GreetingProcessor {
	String processGreeting();
}
