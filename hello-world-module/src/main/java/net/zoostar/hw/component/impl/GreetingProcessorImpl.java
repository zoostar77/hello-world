package net.zoostar.hw.component.impl;

import java.util.concurrent.Callable;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.zoostar.hw.component.GreetingProcessor;
import net.zoostar.hw.util.PercentCalculator;

@Slf4j
@Getter
@RequiredArgsConstructor
public class GreetingProcessorImpl implements GreetingProcessor, Callable<String> {

	public static final int MAX = 100;
	
	private final String name;
	
	private PercentCalculator percentCalculator = new PercentCalculator(40);
	
	@Override
	public String processGreeting() {
		log.info("Started processing: {}", name);
		int x = 0;
		float progress = percentCalculator.calculate(x+=10);
		log.info("Completed processing {}: {}%", name, progress);
		long milliseconds = (long)(Math.random() * 10000l);
		progress = percentCalculator.calculate(x+=10);
		log.info("Completed processing {}: {}%", name, progress);
		try {
			Thread.sleep(milliseconds);
			progress = percentCalculator.calculate(x+=10);
			log.info("Completed processing {}: {}%", name, progress);
		} catch (InterruptedException e) {
			milliseconds = -1l;
			log.warn(e.getMessage());
			Thread.currentThread().interrupt();
		} finally {
			progress = percentCalculator.calculate(x+=10);
			log.info("Completed processing {}: {}%", name, progress);
		}
		return "Hello " + name;
	}

	@Override
	public String call() throws Exception {
		return processGreeting();
	}

}
