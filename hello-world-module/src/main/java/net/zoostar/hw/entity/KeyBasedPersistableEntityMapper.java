package net.zoostar.hw.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import net.zoostar.hw.request.UserRequest;
import net.zoostar.kd.domain.KeyBasedPersistable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(name = "user", value = UserRequest.class) })
public interface KeyBasedPersistableEntityMapper<P extends KeyBasedPersistable<K, I>, K, I> {
	P toKeyBasedPersistable();
}
