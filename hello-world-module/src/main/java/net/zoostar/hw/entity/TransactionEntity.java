package net.zoostar.hw.entity;

import javax.persistence.Id;

import org.springframework.data.domain.Persistable;

import lombok.Getter;
import net.zoostar.hw.model.Transaction;

@Getter
public class TransactionEntity extends Transaction implements Persistable<String> {

	@Id
	private String id;

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}

}
