package net.zoostar.hw.entity;

public interface PersistableKeyMapper<T> {
	T toKey();
}
