package net.zoostar.hw.entity;

import java.util.Objects;

import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.zoostar.hw.model.User;
import net.zoostar.kd.domain.KeyBasedPersistable;

@Getter
@Setter
@NoArgsConstructor
public class UserEntity extends User implements KeyBasedPersistable<String, String> {

	@Id
	private String id;

	public UserEntity(User user) {
		setEmail(user.getEmail());
		setName(user.getName());
		setUsername(user.getUsername());
	}

	@Override
	@Transient
	@JsonIgnore
	@NotEmpty(message = "Required key field {username} may not be empty!")
	public String getKey() {
		return getUsername();
	}
	
	@Override
	@Email(message = "Value must be a well-formed email address")
	public String getEmail() {
		return super.getEmail();
	}
	
	@Override
	@JsonIgnore
	public boolean isNew() {
		return !StringUtils.hasText(id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getKey());
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (!(that instanceof UserEntity)) {
			return false;
		}
		UserEntity other = (UserEntity) that;
		return Objects.equals(getKey(), other.getKey());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserEntity [");
		if (StringUtils.hasText(id)) {
			builder.append("id=").append(id).append(", ");
		}
		builder.append("super()=").append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
