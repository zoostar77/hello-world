package net.zoostar.hw.request.shapes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Square implements Shape {

	private double length;
	
	@Override
	public double area() {
		return getLength() * getLength();
	}

}
