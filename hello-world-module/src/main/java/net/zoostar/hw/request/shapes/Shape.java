package net.zoostar.hw.request.shapes;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({
	@JsonSubTypes.Type(name = "square", value = Square.class),
	@JsonSubTypes.Type(name = "rectangle", value = Rectangle.class),
	@JsonSubTypes.Type(name = "circle", value = Circle.class)
})
public interface Shape {
	double area();
}
