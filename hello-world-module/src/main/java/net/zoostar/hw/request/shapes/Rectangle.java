package net.zoostar.hw.request.shapes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Rectangle extends Square {

	private double width;
	
	@Override
	public double area() {
		return getLength() * getWidth();
	}
}
