package net.zoostar.hw.request;

import net.zoostar.hw.entity.PersistableIdMapper;

public class StringIdRequest extends StringRequest implements PersistableIdMapper<String> {

	public StringIdRequest() {
		super();
	}
	
	public StringIdRequest(String id) {
		super(id);
	}

	@Override
	public String toId() {
		return getValue();
	}

}
