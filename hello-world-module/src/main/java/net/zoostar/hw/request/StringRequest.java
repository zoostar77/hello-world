package net.zoostar.hw.request;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class StringRequest implements Comparable<StringRequest> {

	@NotEmpty(message = "Required field may not be blank!")
	private String value;
	
	@Override
	public String toString() {
		return value;
	}

	@Override
	public int compareTo(StringRequest o) {
		return getValue().compareTo(o.getValue());
	}

}
