package net.zoostar.hw.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonTypeName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.zoostar.hw.entity.KeyBasedPersistableEntityMapper;
import net.zoostar.hw.entity.UserEntity;
import net.zoostar.hw.model.User;

@Getter
@NoArgsConstructor
@JsonTypeName("user")
@ToString(callSuper = true)
public class UserRequest extends User implements KeyBasedPersistableEntityMapper<UserEntity, String, String> {
	
	public UserRequest(User user) {
		setEmail(user.getEmail());
		setName(user.getName());
		setUsername(user.getUsername());
	}
	
	@Override
	@NotEmpty(message = "Required key field {username} may not be empty!")
	public String getUsername() {
		return super.getUsername();
	}
	
	@Override
	@Email(message = "Value must be a well-formed email address")
	public String getEmail() {
		return super.getEmail();
	}
	
	@Override
	public UserEntity toKeyBasedPersistable() {
		var entity = new UserEntity();
		entity.setEmail(getEmail());
		entity.setName(getName());
		entity.setUsername(getUsername());
		return entity;
	}

}
