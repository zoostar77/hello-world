package net.zoostar.hw.request.shapes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Circle implements Shape {

	public static final double PI = 22/7;
	
	private float diameter;
	
	@Override
	public double area() {
		return getDiameter() * PI;
	}

}
