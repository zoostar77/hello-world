package net.zoostar.hw.dao.kd;

import net.zoostar.hw.entity.UserEntity;
import net.zoostar.kd.data.KeyBasedRepository;

public interface UserRepository extends KeyBasedRepository<UserEntity, String, String> {
	
}
