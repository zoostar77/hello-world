package net.zoostar.hw.dao.kd.impl;

import org.springframework.stereotype.Repository;

import lombok.Generated;
import net.zoostar.hw.dao.kd.UserRepository;
import net.zoostar.hw.entity.UserEntity;
import net.zoostar.kd.data.AbstractKeyBasedRepository;

@Generated
@Repository
public class UserRepositoryImpl extends AbstractKeyBasedRepository<UserEntity, String, String>
		implements UserRepository {

}
